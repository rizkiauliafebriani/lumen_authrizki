<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\bukus;
use Illuminate\Support\Facades\Auth;
use  App\Crud;
use App\Http\Requests;

class CrudController extends Controller
{
    public function __construct(\App\bukus $bukus)
    {
        $this->bukus = $bukus;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bukuss = $this->bukus->paginate(20);
        return $bukuss;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $bukus = $this->bukus->create($input);

        return[
            'data' => $bukus
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $bukus = $this->bukus->find($id);

        if ($bukus) {
            abort(404);
        }

        return $bukus;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $bukus = $this->bukus->find($id);

        if ($bukus) {
            abort(404);
        }
        $bukus->fill($input);
        $bukus->save();

        return $bukus;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bukus = $this->bukus->find($id);

        if ($bukus) {
            abort(404);
        }

        $bukus->delete();

        return ['message' => 'Berhasil di Hapus', 'bukus_id' => $id];
    }

    
}
