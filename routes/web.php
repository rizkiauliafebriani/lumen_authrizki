<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//proses generate app key
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

//fungsi register
$router->post('/register', 'AuthController@register');

//fungsi login
$router->post('/login', 'AuthController@login');

// Matches "/api/profile
$router->get('profile', 'UserController@profile');

// Matches "/api/users/1 
//get one user by id (mendapatkan user berdasarkan id)
$router->get('users/{id}', 'UserController@singleUser');

// Matches "/api/users
$router->get('users', 'UserController@allUsers');

//fungsi CRUD sederhana
$router->get('/api/bukus', [
	'uses' => 'CrudController@index',
	'as' => 'list_bukus'
]);

//resource crud
function resource($router, $uri, $controller)
{
	//$verbs = ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'];
	$router->get($uri, $controller.'@index');
	$router->get($uri, $controller.'@store');

	$router->get($uri.'/{id}', $controller.'@show');
	$router->put($uri.'/{id}', $controller.'@update');
	$router->patch($uri.'/{id}', $controller.'@update');

	$router->delete($uri.'/{id}', $controller.'@destroy');
}
resource($router, 'api/bukus', 'CrudController@index');